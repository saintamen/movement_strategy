package com.sda.dp.strategy.movement;

public class LineStyleMovement implements IMovementStyle {
    public Point getNewPoint() {
        return new Point(1.0, 0.0);
        // oznacza ruch o 1 w prawo i o 0.0 w górę/dół
    }
}

package com.sda.dp.strategy.movement;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Worm w = new Worm();


        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking){

            String line = scanner.nextLine();
            if(line.equals("1")){
                w.setStyle(new LineStyleMovement());
            }else if (line.equals("2")){
                w.setStyle(new ZigzagMovement());
            }
        }
    }
}

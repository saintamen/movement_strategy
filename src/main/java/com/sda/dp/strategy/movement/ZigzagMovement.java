package com.sda.dp.strategy.movement;

public class ZigzagMovement implements IMovementStyle {

    private boolean direction = true;
    private int counter = 0;

    @Override
    public Point getNewPoint() {

        if (direction) { // przesuwamy sie w dol
            counter++;
            if (counter == 50) { // co 50
                counter = 0; // resetuje counter
                direction = false; // zmieniam kierunek
            }
            return new Point(1.0, -1.0);
        } else {
            counter++;
            if (counter == 50) {
                counter = 0;
                direction = true;
            }
            return new Point(1.0, 1.0);
        }

    }
}

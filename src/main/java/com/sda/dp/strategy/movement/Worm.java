package com.sda.dp.strategy.movement;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Worm implements Runnable{
    private double x = 0.0;
    private double y = 0.0;

    private IMovementStyle style = new ZigzagMovement();

    private ExecutorService watek = Executors.newSingleThreadExecutor();

    public Worm() {
        watek.submit(this);
    }

    public void generateMove() {
        Point whereTo = style.getNewPoint();
        this.x += whereTo.getX();
        this.y += whereTo.getY();

        saveToFile();
    }

    public void setStyle(IMovementStyle style) {
        this.style = style;
    }

    private void saveToFile() {
        try (PrintWriter writer = new PrintWriter(new FileWriter("data.txt", true))) {
            writer.println(this.x + ";" + this.y);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean isWorking = true;

        while (isWorking){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            generateMove();
        }
    }
}

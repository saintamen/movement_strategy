package com.sda.dp.strategy.movement;

public interface IMovementStyle {

    Point getNewPoint();
}
